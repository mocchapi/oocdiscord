import os
import random
import asyncio
import requests

import tweepy
import discord
from discord.ext import commands
from tweepyauth import auto_authenticate

client = discord.Client()
bot = commands.Bot(command_prefix=['ooc!'], case_insensitive=True)

@bot.event
async def on_connect():
	print('Connected to discord')

@bot.event
async def on_ready():
	while True:
		await update_discord_status()
		await asyncio.sleep(60*60)

async def update_discord_status():
	print('updating status')
	try:
		followercount = twit.me().followers_count
		activity = discord.Activity(name=f'{followercount} followers', type=discord.ActivityType.watching)
		await bot.change_presence(activity=activity)
	except Exception as e:
		print(e)


async def get_media(url,destination='./media/'):
	## This functions handles the downloading of images
	# im not entirely sure if this works with imgur but i.redd.it + direct urls to images are confirmed working
	if destination.endswith('/') == False:
		destination = f'{destination}/'
	print(f'Downloading "{url}" to directory {destination}')
	filename = url.split('/')[-1]
	# not a file format twitter supports? yeet it away babey
	# YES i know this is bad coding but its only a few cases so it doesnt really matter shush
	if not filename.lower().endswith('jpg') and not filename.lower().endswith('png') and not filename.lower().endswith('jpeg') and not filename.lower().endswith('gif'):
		raise ValueError(f'excpeted url of type "jpg" "png" "jpeg" or "gif", got "{filename}"')

	# this loads the image into memory
	image = requests.get(url)
	# and here we put it all in a file
	# using .content & open(..,'wb') is important, since these are not UTF-8 text files, they are byte files
	with open(f'{destination}{filename}','wb') as f:
		f.write(image.content)
	print(f'downloaded {destination}{filename}')
	return f'{destination}{filename}'


async def handle_post(ctx,mediaUrls,text='',comment=True):
	if type(mediaUrls) != list: mediaUrls = [mediaUrls]
	print('text:',text)
	output = await ctx.channel.send('Downloading images...')
	try:
		media_id_list = []
		files = set()
		for item in mediaUrls:
			file = await get_media(item)
			files.add(file)
			await output.edit(content='Uploading image to twitter...')
			twitfile = twit.media_upload(file)
			media_id_list.append(twitfile.media_id)
		await output.edit(content='Making tweet...')
		if text:
			tweet = twit.update_status(status=text,media_ids=media_id_list)
		else:
			tweet = twit.update_status(media_ids=media_id_list)
		await ctx.channel.send(content=f'Made tweet: https://twitter.com/{tweet.author.screen_name}/status/{tweet.id_str}')
		if comment:
			await output.edit(content=f'Adding reply...')
			twit.update_status(f'Post submitted by {allowedUsers[ctx.author.id]}',in_reply_to_status_id=tweet.id,auto_populate_reply_metadata=True)
		await output.edit(content='Removing images...')
		for file in files:
			os.remove(file)
		await output.edit(content='Done')
		await output.delete(delay=5)
	except Exception as e:
		await output.edit(content=f'An error occured: {e}')
		return None


@bot.command(alias=['delete'],name='remove',brief='Removes a twitter post')
async def remove(ctx,*tweets):
	for tweet in tweets:
		try:
			tweet_id = tweet.split('/')[-1]
			twit.destroy_status(tweet_id)
			await ctx.channel.send('Tweet deleted')
		except Exception as e:
			await ctx.channel.send(f'An error occured: {e}')


@bot.command(name='era',brief='start a new era')
async def era(ctx,*,eraname):
	if len(eraname) <= 20:
		fulltitle = random.choice([f'And so starts the era of {eraname}',f'The era of {eraname} has come'])
		twit.update_profile(eraname)
		tweet = twit.update_status(fulltitle)
		await ctx.channel.send(content=f'Made tweet: https://twitter.com/{tweet.author.screen_name}/status/{tweet.id_str}')
	else:
		await ctx.send(f'Era may not be longer than 20 characters ({len(eraname)}/20)\nMaybe try "{eraname[:19]}"')



@bot.command(aliases=['p'],name='post',brief='Posts image to twitter. use `-a` to post anonymously',description='Posts image to twitter')
async def post(ctx,*args):
	text = ''
	mediaUrls = []
	# print(dir(ctx))
	comment = True
	for item in args:
		if item.startswith('https://'):
			mediaUrls.append(item)
		elif item == '-a':
			comment = False
		else:
			text += ' '+str(item)
	if ctx.message.attachments:
		for item in ctx.message.attachments:
			mediaUrls.append(str(item.url))
	if len(mediaUrls) > 4:
		mediaUrls = mediaUrls[:3]
	await handle_post(ctx,mediaUrls,text=text.strip(),comment=comment)

@bot.command(name='refresh',description='Refreshes the user whitelist')
async def refresh(ctx):
	try:
		global allowedUsers
		allowedUsers = getWhitelist()
		await ctx.channel.send(f'Refreshed whitelist')
	except Exception as e:
		await ctx.channel.send(f'An error occured: "{e}"')

@bot.event
async def on_message(msg):
	if msg.author.id in allowedUsers:
		print(f'[{msg.guild}] (#{msg.channel}) {msg.author}: {msg.clean_content}')
		try:
			await bot.process_commands(msg)
		except Exception as e:
			await msg.channel.send('An error occured: {e}')
	






def getWhitelist(filename='./user_whitelist.txt'):
	users = {}
	with open(filename,'r') as f:
		for line in f.read().split('\n'):
			if not line.startswith('#') and line != '':
				userid,username = line.split(';')
				users[int(userid)] = username 
	return users

if __name__ == '__main__':
	# log.basicConfig(format='%(asctime)s %(levelname)-8s [%(filename)s:%(funcName)s:%(lineno)d] %(message)s',
 #    datefmt='%H:%M:%S',
 #    level=log.INFO)
	global allowedUsers
	global twit
	allowedUsers = getWhitelist()

	twit = auto_authenticate()
	try:
		print(f'logged in to twitter as @{twit.me().screen_name}')
	except tweepy.error.RateLimitError:
		print(f'Twitter ratelimited!, waiting for 15 min')
		time.sleep(15*60)

	with open('discord_token.txt','r') as f:
		discord_token = f.read()
	bot.run(discord_token)
